#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):
    def __init__(self):
        self.lista = []
        self.etiqueta1 = ""
        self.etiqueta2 = ""
        self.etiqueta3 = ""
        self.etiqueta4 = ""
        self.etiqueta5 = ""

    def startElement(self, name, attrs):
        if name == 'root-layout':
            self.etiqueta1 = attrs.get("width", '')
            self.etiqueta2 = attrs.get("height", '')
            self.etiqueta3 = attrs.get("background-color", '')
            self.lista.append({'etiqueta': name, "width": self.etiqueta1, "height": self.etiqueta2, "background-color": self.etiqueta3})
        elif name == 'region':
            self.etiqueta1 = attrs.get("id", '')
            self.etiqueta2 = attrs.get("top", '')
            self.etiqueta3 = attrs.get("bottom", '')
            self.etiqueta4 = attrs.get("left", '')
            self.etiqueta5 = attrs.get("right", '')
            self.lista.append({'etiqueta': name, "id": self.etiqueta1, "top": self.etiqueta2, "bottom": self.etiqueta3, "left": self.etiqueta4, "right": self.etiqueta5})
        elif name == 'img':
            self.etiqueta1 = attrs.get("src", '')
            self.etiqueta2 = attrs.get("region", '')
            self.etiqueta3 = attrs.get("begin", '')
            self.etiqueta4 = attrs.get("dur", '')
            self.lista.append({'etiqueta': name, "src": self.etiqueta1, "region": self.etiqueta2, "begin": self.etiqueta3, "dur": self.etiqueta4})

        elif name == 'audio':
            self.etiqueta1 = attrs.get("src", '')
            self.etiqueta2 = attrs.get("begin", '')
            self.etiqueta3 = attrs.get("region", '')
            self.lista.append({'etiqueta': name, "src": self.etiqueta1, "begin": self.etiqueta2, "region": self.etiqueta3})

        elif name == 'textstream':
            self.etiqueta1 = attrs.get("src", '')
            self.etiqueta2 = attrs.get("region", '')
            self.lista.append({'etiqueta': name, "src": self.etiqueta1, "region": self.etiqueta2})

    def get_tags(self):
        return self.lista


if __name__ == "__main__":
    parser = make_parser()  # lee linea a line y cuando detetcta un elemento llama al manejador
    sHandler = SmallSMILHandler()
    parser.setContentHandler(sHandler)
    parser.parse(open('karaoke.smil'))
    etiquetas = sHandler.get_tags()
    print(etiquetas)
