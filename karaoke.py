from smallsmilhandler import SmallSMILHandler
from xml.sax import make_parser
import json
import sys
import urllib.request


class KaraokeLocal(SmallSMILHandler):
    def __init__(self, fichero):
        parser = make_parser()
        sHandler = SmallSMILHandler()
        parser.setContentHandler(sHandler)
        parser.parse(open(fichero))
        self.etiquetas = sHandler.get_tags()

    def __str__(self):
        for atributos in self.etiquetas:
            for etiqueta in atributos:
                if atributos[etiqueta] != '':
                    print(etiqueta, '=', atributos[etiqueta], end='\t')
            print('\n')
        return(atributos[etiqueta])

    def to_json(self, fichero):
        ficherojson = fichero.replace('.smil', '.json')
        with open(ficherojson, 'w') as myfile:
            json.dump(self.etiquetas, myfile)

    def do_local(self):
        # urllib = descarga el archivo.
        # urlretrieve = copia en un archivo local.
        for atributos in self.etiquetas:
            for etiqueta in atributos:
                if etiqueta == 'src':
                    direccion = atributos[etiqueta]
                    if direccion.startswith('http://'):
                        guardar = direccion.split('/')[-1]
                        urllib.request.urlretrieve(direccion, guardar)
                        print('Descargado')

if __name__ == "__main__":
    try:
        fichero = (sys.argv[1])
        karaoke = KaraokeLocal(fichero)
        print(karaoke)
        karaoke.to_json(fichero)
        karaoke.do_local()
        karaoke.to_json('local.json')
        print(karaoke)
    except:
        print("Usage: python3 karaoke.py file.smil.")
